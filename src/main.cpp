#include <time.h>
#include <stdlib.h>

#include <SDL/SDL.h>

#include <iostream>
#include <complex>

#include <gmpxx.h>

#define ULTRA_PRECISION     100000
#define HIGH_PRECISION      10000
#define MEDIUM_PRECISION    1000
#define LOW_PRECISION       100

using namespace std;

Uint32 *couleurs;
size_t precision;

typedef struct complex_multi_prec {
    mpf_class real;
    mpf_class imag;
} ComplexMultiPrec;

/* ********************************************************************* */
/*obtenirPixel : permet de récupérer la couleur d'un pixel
Paramètres d'entrée/sortie :
SDL_Surface *surface : la surface sur laquelle on va récupérer la couleur d'un pixel
int x : la coordonnée en x du pixel à récupérer
int y : la coordonnée en y du pixel à récupérer

Uint32 resultat : la fonction renvoie le pixel aux coordonnées (x,y) dans la surface
*/
Uint32 obtenirPixel(SDL_Surface *surface, int x, int y)
{
    /*nbOctetsParPixel représente le nombre d'octets utilisés pour stocker un pixel.
    En multipliant ce nombre d'octets par 8 (un octet = 8 bits), on obtient la profondeur de couleur
    de l'image : 8, 16, 24 ou 32 bits.*/
    int nbOctetsParPixel = surface->format->BytesPerPixel;
    /* Ici p est l'adresse du pixel que l'on veut connaitre */
    /*surface->pixels contient l'adresse du premier pixel de l'image*/
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * nbOctetsParPixel;

    /*Gestion différente suivant le nombre d'octets par pixel de l'image*/
    switch(nbOctetsParPixel)
    {
        case 1:
            return *p;

        case 2:
            return *(Uint16 *)p;

        case 3:
            /*Suivant l'architecture de la machine*/
            if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
                return p[0] << 16 | p[1] << 8 | p[2];
            else
                return p[0] | p[1] << 8 | p[2] << 16;

        case 4:
            return *(Uint32 *)p;

        /*Ne devrait pas arriver, mais évite les erreurs*/
        default:
            return 0;
    }
}

/* ********************************************************************* */
/*definirPixel : permet de modifier la couleur d'un pixel
Paramètres d'entrée/sortie :
SDL_Surface *surface : la surface sur laquelle on va modifier la couleur d'un pixel
int x : la coordonnée en x du pixel à modifier
int y : la coordonnée en y du pixel à modifier
Uint32 pixel : le pixel à insérer
*/
void definirPixel(SDL_Surface *surface, int x, int y, Uint32 pixel)
{
    /*nbOctetsParPixel représente le nombre d'octets utilisés pour stocker un pixel.
    En multipliant ce nombre d'octets par 8 (un octet = 8 bits), on obtient la profondeur de couleur
    de l'image : 8, 16, 24 ou 32 bits.*/
    int nbOctetsParPixel = surface->format->BytesPerPixel;
    /*Ici p est l'adresse du pixel que l'on veut modifier*/
    /*surface->pixels contient l'adresse du premier pixel de l'image*/
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * nbOctetsParPixel;

    /*Gestion différente suivant le nombre d'octets par pixel de l'image*/
    switch(nbOctetsParPixel)
    {
        case 1:
            *p = pixel;
            break;

        case 2:
            *(Uint16 *)p = pixel;
            break;

        case 3:
            /*Suivant l'architecture de la machine*/
            if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
            {
                p[0] = (pixel >> 16) & 0xff;
                p[1] = (pixel >> 8) & 0xff;
                p[2] = pixel & 0xff;
            }
            else
            {
                p[0] = pixel & 0xff;
                p[1] = (pixel >> 8) & 0xff;
                p[2] = (pixel >> 16) & 0xff;
            }
            break;

        case 4:
            *(Uint32 *)p = pixel;
            break;
    }
}

void fractale(const double &centre_x, const double &centre_y, const double &zoom, SDL_Surface *surface)
{
  complex<double> origin, z, c, nouveau;

  short int x, y;
  size_t couleur;

  cerr << "Computing image..";

  origin.real() = centre_x;
  origin.imag() = centre_y;

  #pragma omp parallel for num_threads(5) private(c,z,couleur,nouveau,x,y)
  for (y = 0; y < surface->h; y++)
  {
    for (x = 0; x < surface->w; x++)
    {
        // Calcul le nombre complexe situé en (x,y)
        c.real() = (origin.real() + ( (double) (x-(surface->w/2)) / (double) surface->w ) ) / zoom;
        c.imag() = (origin.imag() + ( (double) (y-(surface->h/2)) / (double) surface->h ) ) / zoom;

        z.real() = z.imag() = 0.0; // Point de départ

        couleur = 0;

        while(couleur<precision)
        {
            nouveau.real() = z.real() * z.real() - z.imag() * z.imag() + c.real();
            nouveau.imag() = 2 * z.real() * z.imag() + c.imag();

            z.real() = nouveau.real();
            z.imag() = nouveau.imag();

            couleur++;

            if (nouveau.real()*nouveau.real() + nouveau.imag()*nouveau.imag() > 4) break;
        }

        // Mise à jour de la couleur
        if (couleur<precision)
        {
            definirPixel(surface, x, y, couleurs[couleur]);
        }
        else
        {
            definirPixel(surface, x, y, (Uint32)0);
        }
    }
  }

  cerr << " done" << endl;
}

void fractaleMultiPrec(const double &centre_x, const double &centre_y, const double &zoom, SDL_Surface *surface)
{
  ComplexMultiPrec origin, offset, z, c, tmp;
  size_t nbIteration;
  short int x, y;

  cerr << "Computing image.. ";

  origin.real = mpf_class(centre_x);
  origin.imag = mpf_class(centre_y);

  mpf_class zoom_multiPrec(zoom);

  #pragma omp parallel for num_threads(5) private(c, z, tmp, nbIteration, x, y)
  for (y = 0; y < surface->h; y++)
  {
    for (x = 0; x < surface->w; x++)
    {
        offset.real = mpf_class( (double) (x - surface->w / 2.0) / (double) surface->w );
        offset.imag = mpf_class( (double) (y - surface->h / 2.0) / (double) surface->h );

        c.real = (origin.real + offset.real ) / zoom_multiPrec;
        c.imag = (origin.imag + offset.imag ) / zoom_multiPrec;

        z.imag = mpf_class(0);
        z.real = mpf_class(0);

        for (nbIteration = 0; nbIteration <= precision; nbIteration++) {
            tmp.real = z.real * z.real - z.imag * z.imag + c.real;
            tmp.imag = 2 * z.real * z.imag + c.imag;

            z = tmp;

            if (z.real * z.real + z.imag * z.imag > 4) {
                break;
            }
        }

        if (nbIteration < precision) {
            definirPixel(surface, x, y, couleurs[nbIteration]);
        }
        else
        {
            definirPixel(surface, x, y, (Uint32)0);
        }
    }
  }

  cerr << "done" << endl;
}

Uint32 *genTabColor(const size_t &taille, const SDL_PixelFormat *format)
{
    Uint32 *tab = new Uint32[taille];

    for (size_t i=0; i<taille; ++i)
    {
        tab[i] = SDL_MapRGB(format, rand() % 256, 0, rand() % 256);
    }

    return tab;
}

void setPrecision(unsigned int p) {
    precision = p;
}

int main(/*int argc, char **argv*/)
{
    bool run = true;
    bool multiPrec = false;
    SDL_Event event;
    SDL_Surface *ecran;
    double centreX = 0, centreY = 0, zoom = 0.1;

    srand(time(NULL));

    SDL_Init(SDL_INIT_VIDEO);

    SDL_EnableKeyRepeat(10, 100);

    if (SDL_Init(SDL_INIT_VIDEO) == -1)
    {
        cerr << "Erreur d'initialisation de la SDL : " <<  SDL_GetError() << endl;
        exit(EXIT_FAILURE);
    }

    ecran = SDL_SetVideoMode(200, 200, 32, SDL_HWSURFACE /*| SDL_FULLSCREEN*/ | SDL_RESIZABLE | SDL_DOUBLEBUF);

    couleurs = genTabColor(ULTRA_PRECISION, ecran->format);

    setPrecision(LOW_PRECISION);

    while (run)
    {
		SDL_LockSurface(ecran);

        if (multiPrec) {
            fractaleMultiPrec(centreX*zoom, centreY*zoom, zoom, ecran);
        } else {
            fractale(centreX*zoom, centreY*zoom, zoom, ecran);
        }

        SDL_UnlockSurface(ecran);

        SDL_Flip(ecran);
		
        SDL_WaitEvent(&event);

        while (event.type != SDL_KEYDOWN && event.type != SDL_QUIT) {
            SDL_WaitEvent(&event);
        }

        switch(event.type)
        {
            case SDL_QUIT:
                run = false;
            break;

            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                    case SDLK_UP:
                        centreY -= 0.1 / zoom;
                    break;

                    case SDLK_LEFT:
                        centreX -= 0.1 / zoom;
                    break;

                    case SDLK_DOWN:
                        centreY += 0.1 / zoom;
                    break;

                    case SDLK_RIGHT:
                        centreX += 0.1 / zoom;
                    break;

                    case SDLK_r:
                        zoom += zoom;
                    break;

                    case SDLK_f:
						zoom -= zoom / 2;
                    break;
                    
                    case SDLK_l: 
						setPrecision(LOW_PRECISION);
                    break;
                    
                    case SDLK_m: 
						setPrecision(MEDIUM_PRECISION);
                    break;
                    
                    case SDLK_h: 
						setPrecision(HIGH_PRECISION);
                    break;
                    
                    case SDLK_u: 
						setPrecision(ULTRA_PRECISION);
                    break;

                    case SDLK_t:
                        multiPrec = true;
                    break;

                    case SDLK_g:
                        multiPrec = false;
                    break;

                    case SDLK_c:
                        delete couleurs;
                        couleurs = genTabColor(ULTRA_PRECISION, ecran->format);
                    break;

                    case SDLK_ESCAPE:
                        run = false;
                    break;

                    default: 
                    break;
                }
        }
    }

    SDL_Quit();

    delete couleurs;

    return 0;
}
