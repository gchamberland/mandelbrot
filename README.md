# README #

A little program designed to explore the Mandelbrot set.

### How to install it : ###

You first need to install the OpenMP and SDL, and GNU MP librairies.

You also need g++, make and cMake.

Then open a command prompt (Bash only) and type :

    git clone https://gchamberland@bitbucket.org/gchamberland/mandelbrot.git mandelbrot
    cd mandelbrot; chmod +x build.sh
    ./build.sh

Now the program is built, place it wherever you want. You're done !

To launch the program :

    ./build/mandelbrot

### Key binding ###

Move the point of view :

    UP: move up
    LEFT: move left
    DOWN: move down
    RIGHT: move right

Zoom into the set :

    R: zoom
    F: zoom out

Increase the precision of the representation :

    L: use low precision, default (100 iterations max.)
    M: use medium precision (1000 iterations max.)
    H: use high precision (10000 iterations max.)
    U: ultra high precision (100000 iterations max.)

    G: use fixed-precision (hardware : faster, limited possibility of zoom), default
    T: use multi-precision (software : unlimited possibility of zoom, slower)

Changes colors of the representation :

    C: compute a new random color table.

### Notes ###

Note that program will be very slower when representation contains a lot of black (values inside the set) because of its computation mode . Indeed, the point is to test that, for a given value, a certain mathematic sequence is a loop. For most values outside the set, a low number of iteration allow to tell that the sequence isn't a loop. But values are considered inside the set only if they reach the maximum number of iteration. That's why the program is slow when there is a lot of black. For more information about the Mandelbrot set, visit [its wikipedia page](https://en.wikipedia.org/wiki/Mandelbrot_set)